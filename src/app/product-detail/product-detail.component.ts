import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import{Products} from '../products';
import {DataService} from '../data.service';
import {BehaviorSubjectService} from '../behavior-subject.service';
import {Cart} from '../cart';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
SKU;
product:Products;
cart:Cart[];

  constructor(private route: ActivatedRoute,private _dataService: DataService, private _BehaviorSubjectService:BehaviorSubjectService,private router: Router) {
    this.SKU = this.route.snapshot.params.SKU;
  }

  ngOnInit() {    
    this._dataService.getProductDetails(this.SKU)
      .subscribe(products => {this.product = products;
      console.log(this.product);});   
  }

  addToCart(){
  
  if(localStorage.getItem("currentUserName") == undefined){
    alert("Please login first");
    this.router.navigate(['/home']);

  }
  else{
  console.log(this.product[0].SKU);  
    this._dataService.addItemsToCart(localStorage.getItem("currentUserName"),this.product[0].SKU,1,this.product[0].Price)
    .subscribe(cart=>{      
      this.cart = cart;          
      this._dataService.getItemsFromCart(localStorage.getItem("currentUserName")).subscribe(
        cart=> {
          this._BehaviorSubjectService.CartQuantity(cart[0].Quantity);  
        }
      );     
      
    },
    error=>{
      console.log(error);
    });
}
  }

}
